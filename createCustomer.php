<?php
    require "class/database.class.php";
    require "class/status.class.php";
    require "class/general.class.php";
    require "class/html.class.php";

    $h=new HTMLPage;

    echo $h->head();
    echo $h->bodystart();
    echo $h->navbar();
    echo $h->aside();
?>
<div class="content-wrapper">
    <section class="content">
        <div class="card">
            <div class="card-body">
                <h3 class="text-info">Insert Customer</h3>
                <form action="saveCustomer.php" method="post">
                    <label class='col-sm-4'>Customer name:
                        <input  name='customer_name' class='form-control' type='text'>
                    </label>
                    <label class='col-sm-4'>Customer email:
                        <input  name='customer_email' class='form-control'  type='text'>
                    </label>
                    <label class='col-sm-4'>Customer phone:
                        <input  name='customer_phone' class='form-control' type='text'>
                    </label>

                    <hr>
                    <button class="btn btn-success">Save</button>  <a href="customer.php" class="btn btn-warning">Back</a>
                </form>                
         
            </div>
        </div>
    </section>
</div>
<?php
    echo $h->footer();
?>
