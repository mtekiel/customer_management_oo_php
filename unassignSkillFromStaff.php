<?php
	require "class/database.class.php";
	require "class/staff.class.php";
	require "class/skill.class.php";
	require "class/html.class.php";
	
	$h=new HTMLPage;
	
	echo $h->head();
	echo $h->bodystart();
	echo $h->navbar();
	echo $h->aside();
	
	$staff = new Staff();

	$unassignSkill = $staff->deleteSkill($_GET['ss_id']);
			
?>

<div class="content-wrapper">
	<section class="content">
		<div class="card">
			<div class="card-body">
				<?php
					echo "<p class='class='alert alert-success'>Skill unassigned from Staff!</p>";
				?>
				<a href= "javascript:history.back()" class="btn btn-primary">Back</a>
			</div>
		</div>
	</section>
</div>
<?php
	echo $h->footer();
?>
