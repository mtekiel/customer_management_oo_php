<?php
	require "class/database.class.php";
	require "class/customer.class.php";
	require "class/html.class.php";
	require "class/general.class.php";

	$h=new HTMLPage;
	
	echo $h->head();
	echo $h->bodystart();
	echo $h->navbar();
	echo $h->aside();	
	
?>
<div class="content-wrapper">
	<section class="content">
		<div class="card">
			<div class="card-body">
				<h3 class="text-info">Customers</h3>
				<?php
					$project = new Customer();
					$all = $project->getAll();
					$general = new General();
					echo $general->newbutton('New Customer','createCustomer.php','info' );
				?>
				<table class="table">
					<thead>
						<th>#</th><th>Name</th><th>Email</th><th>Phone</th><th>Action</th>
					</thead>
					<tbody>
						<?php
							foreach($all as $key=>$row){
								foreach($row as $k=>$cell){
									switch ($k) {
										case 'status':
											echo "<td> $color $cell</span></td>";
											break;
										case "color":
											$color="<span class='badge badge-".$cell ."'>";
											break;
										default:
											echo "<td>$cell</td>";
											break;
									}
									if($k=="customer_id"){
										$id=$cell;
									}
								}
								echo "<td><a href='updateCustomer.php?id=$id' class='btn btn-warning'>Update</a>
									<a href='deleteCustomer.php?id=$id' class='btn btn-danger'>Delete</a></td>";
								echo "</tr>";
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</div>

<?php	
	echo $h->footer();
?>

