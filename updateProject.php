<?php
    require "class/database.class.php";
    require "class/project.class.php";
    require "class/general.class.php";
    require "class/html.class.php";

    $h=new HTMLPage;

    echo $h->head();
    echo $h->bodystart();
    echo $h->navbar();
    echo $h->aside();
?>
<div class="content-wrapper">
    <section class="content">
        <div class="card">
            <div class="card-body">
                <h3 class="text-info">Update Project</h3>
                <form action="doupProject.php?id=<?php echo $_GET['id'];?>" method="post">
                    <?php
                        $project = new Project();
                        $one = $project->getOne($_GET['id']);
                    ?>
                    <input  name='project_id' class='form-control' hidden value='<?php echo $one[0]['project_id'];?>' type='text'>
                    
                    <label class='col-sm-4'>Project name:
                        <input  name='project_name' class='form-control'  value='<?php echo $one[0]['project_name'];?>' type='text'>
                    </label>

                    <label class='col-sm-4'>Project startdate
                        <input  name='project_startdate' class='form-control'  value='<?php echo $one[0]['project_startdate'];?>' type='date'>
                    </label>

                    <label class='col-sm-4'>Project enddate
                        <input  name='project_enddate' class='form-control'  value='<?php echo $one[0]['project_enddate'];?>' type='date'>
                    </label>

                    <label class='col-sm-4'>Customer:
                        <?php
                            echo $project->callCustomer($one[0]['customer_id']);
                        ?>
                    </label>

                    <label class='col-sm-4'>Status:
                        <?php
                            echo $project->callStatus($one[0]['status_id']);
                        ?>
                    </label>

                    <hr>
                    <button class="btn btn-success">Save</button> <a href="index.php" class="btn btn-warning">Back</a>
                </form>
                <hr>
                <?php
                    echo "<h3 class='text-info'>Staff working on project</h3>";
                    $list=$project->listStaff($_GET['id']);
                ?>
                <div class="row">
                    <table class="table col-sm-6">
                        <thead><th>Name</th><th>Deparment</th><th>Role</th><th>Team</th><th>Action</th>
                        </thead>
                        <tbody>
                            <?php
                                foreach($list as $key=>$row){
                                    foreach($row as $k=> $cell){
                                        if($k=="sp_id"){
                                            $ssid=$cell;
                                        }else{
                                            echo "<td>$cell</td>";
                                        }
                                    }                                    
                                    echo "<td> <a href='removeStaff.php?spid=$ssid' class='btn btn-danger'>Remove</a></td>";
                                    echo "</tr>";
                                }
                                
                            ?>
                        </tbody>
                    </table>
                    <form action="addStaff.php?id=<?php echo $_GET['id'];?>" class='col-sm-6' method="post">
                        <?php
                            $staff= $project->callStaff($_GET['id']);                            
                            echo $staff;
                        ?>
                    </form>
                </div>
            </div>    
        </div>    
    </section>
</div>
<?php    
  echo $h->footer();
?>
