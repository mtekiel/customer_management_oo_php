<?php
	require "class/database.class.php";
	require "class/general.class.php";
	require "class/html.class.php";
	require "class/project.class.php";
	
	$h = new HTMLPage;
	echo $h->head();
	echo $h->bodystart();
	echo $h->navbar();
	echo $h->aside();
?>
<!-- This file presents Projects main view  -->
<div class="content-wrapper">
    <section class="content">
    	<div class="card">
        	<div class="card-body">
  				<h3 class="text-info">Projects</h3>	
				<?php
					$project = new Project();
					$all = $project->getAll();
					$general = new General();
					echo $general->newbutton('New Project','createProject.php','info' );
				?>
				<div id="gantt_here" style='width:100%; height:400px;'>
					<table class="table">
						<thead>
							<th>#</th><th>Name</th><th>Start Date</th><th>End Date</th><th>Customer</th><th>Status</th><th>Action</th>
						</thead>
						<tbody>
							<?php
								foreach($all as $key=>$row){
									foreach($row as $k=> $cell){
										switch ($k) {
											case 'status':
												echo "<td> $color $cell</span></td>";
												break;
											case "color":
												$color="<span class='badge badge-".$cell ."'>";
												break;
											default:
												echo "<td>$cell</td>";
												break;
										}
										if($k=="project_id"){
											$id=$cell;
										}
									}
									echo "<td><a href='updateProject.php?id=$id' class='btn btn-warning'>Update</a>
										<a href='deleteProject.php?id=$id' class='btn btn-danger'>Delete</a></td>";
									echo "</tr>";
								}
							?>
						</tbody>
				</table>
				</div>

    	    </div>
	    </div>
    </section>
</div>



<?php
    echo $h->footer();
?>
<script>
	gantt.config.readonly = true;
  	gantt.config.scale_height = 50;
  	gantt.config.scales = [
  		{unit: "month", step: 1, format: "%F, %Y"},
  		{unit: "day", step: 1, format: "%j, %D"}
  	];
	var tasks = {
		data: [
		    <?php echo $general->gantt();?>
		]
		};
    gantt.config.columns = [
    	{name:"text",       label:"Task name",  width:"150" },
    	{name:"start_date", label:"Start time", align:"center" }

	];

    gantt.attachEvent("onTaskDblClick", function(id,e){
      	window.location.href = 'updateProject.php?id='+id;

    });

	gantt.init("gantt_here");
	gantt.parse(tasks);

</script>
