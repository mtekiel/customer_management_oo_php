<?php
    require "class/database.class.php";
    require "class/html.class.php";
    require "class/availability.class.php";
    require "class/staff.class.php";
    
    $h=new HTMLPage;
   
    echo $h->head();
    echo $h->bodystart();
    echo $h->navbar();
    echo $h->aside();
   
    $availability= new Availability();
    $staff= new Staff();
?>
<div class="content-wrapper">
    <section class="content">
        <div class="card">
            <div class="card-body">
                <h3 class="text-info">Availability</h3>
                <table class="table">
                    <thead>
                        <th>Staff</th>
                        <?php
                            echo $availability->generatethDate(6);
                        ?>
                    </thead>
                    <tbody>
                        <?php
                            echo  ( $availability->generateLine());
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

<?php
    echo $h->footer();
?>
