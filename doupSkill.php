<?php
  require "class/database.class.php";
  require "class/skill.class.php";
  require "class/html.class.php";
  $h=new HTMLPage;
  echo $h->head();
  echo $h->bodystart();
  echo $h->navbar();
  echo $h->aside();
  $skill= new Skill();

?>

   <div class="content-wrapper">
     <section class="content">
       <div class="card">
         <div class="card-body">
           <?php
              $skill->update($_GET['id'],$_POST);

              echo "<p class='class='alert alert-success'>Skill updated with success!</p>";
            ?>
            <a href= "javascript:history.back()" class="btn btn-primary">Back</a>
            </div>
          </div>
        </section>
      </div>
<?php
echo $h->footer();
?>
