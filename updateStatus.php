<?php
    require "class/database.class.php";
    require "class/status.class.php";
    require "class/general.class.php";
    require "class/html.class.php";

    $h=new HTMLPage;

    echo $h->head();
    echo $h->bodystart();
    echo $h->navbar();
    echo $h->aside();
?>
<div class="content-wrapper">
    <section class="content">
        <div class="card">
            <div class="card-body">
                <h3 class="text-info">Update Status</h3>
                <form action="doupStatus.php?id=<?php echo $_GET['id'];?>" method="post">
                <?php
                    $status= new Status();
                    $one=$status->getOne($_GET['id']);
                    $general= new General();
                    //var_dump ($one);
                ?>
                    <input  name='status_id' class='form-control' hidden value='<?php echo $one[0]['status_id'];?>' type='text'>

                    <label class='col-sm-4'>Status:<input  name='status' class='form-control'  value='<?php echo $one[0]['status'];?>' type='text'>
                    </label>
                    <label class='col-sm-4'>Color:
                        <select class="form-control" name="color">
                            <?php
                                echo $general->color($one[0]['color']);
                            ?>
                        </select>
                    </label>
                    <hr>
                    <button class="btn btn-success">Save</button> <a href="status.php" class="btn btn-warning">Back</a>
                </form>
            </div>    
        </div>
    </section>
</div>
<?php
    echo $h->footer();
?>
