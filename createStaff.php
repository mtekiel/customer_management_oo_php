<?php
	require "class/database.class.php";
	require "class/status.class.php";
	require "class/general.class.php";

	require "class/html.class.php";
	$h=new HTMLPage;
	echo $h->head();
	echo $h->bodystart();
	echo $h->navbar();
	echo $h->aside();
?>
<div class="content-wrapper">
	<section class="content">
		<div class="card">
			<div class="card-body">
				<h3 class="text-info">Insert Staff</h3>
				<form action="saveStaff.php" method="post">
					<label class='col-sm-4'>Staff name:
						<input  name='staff_name' class='form-control' type='text'>
					</label>
					<label class='col-sm-4'>Staff department:
						<input  name='staff_department' class='form-control'  type='text'>
					</label>
					<label class='col-sm-4'>Staff role:
						<input  name='staff_role' class='form-control' type='text'>
					</label>
					<label class='col-sm-4'>Staff team:
						<input  name='staff_team' class='form-control' type='text'>
					</label>
					<hr>
					<button class="btn btn-success">Save</button>  <a href="staff.php" class="btn btn-warning">Back</a>
				</form>
			</div>
		</div>
	</section>
</div>
<?php
	echo $h->footer();                  
?>
