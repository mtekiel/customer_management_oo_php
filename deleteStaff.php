<?php
    require "class/database.class.php";
    require "class/staff.class.php";
    require "class/html.class.php";
    require "class/project.class.php";

   
    $h=new HTMLPage;
   
    echo $h->head();
    echo $h->bodystart();
    echo $h->navbar();
    echo $h->aside();
   
    $staff = new Staff();
    $staff->deletequery($_GET['id']);

    $project = new Project();
    $project->deleteFromStaffToProjectByStaff($_GET['id']);
?>

<div class="content-wrapper">
    <section class="content">
        <div class="card">
            <div class="card-body">
                <?php
                    echo "<p class='class='alert alert-success'>Staff deleted with success!</p>";
                ?>
                <a href= "javascript:history.back()" class="btn btn-primary">Back</a>
            </div>
        </div>
    </section>
</div>
<?php
    echo $h->footer();
?>
