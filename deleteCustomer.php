<?php
    require "class/database.class.php";
    require "class/customer.class.php";
    require "class/html.class.php";
    
	$h=new HTMLPage;
    
	echo $h->head();
    echo $h->bodystart();
    echo $h->navbar();
    echo $h->aside();
    
	$customer = new Customer();	
    $customer->deleteCustomer($_GET['id']);	       
?>
    
<div class="content-wrapper">
	<section class="content">
		<div class="card">
			<div class="card-body">
				<?php
					echo "<p class='class='alert alert-success'>Customer deleted with success!</p>";
				?>
				<a href= "javascript:history.back()" class="btn btn-primary">Back</a>
			</div>
		</div>
	</section>
</div>
<?php
	echo $h->footer();
?>
