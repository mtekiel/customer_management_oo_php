<?php
	require "class/database.class.php";
	require "class/project.class.php";
	require "class/general.class.php";

	require "class/html.class.php";
	$h=new HTMLPage;

	echo $h->head();
	echo $h->bodystart();
	echo $h->navbar();
	echo $h->aside();
	
	$project= new Project();
?>
<div class="content-wrapper">
	<section class="content">
		<div class="card">
			<div class="card-body">
				<h3 class="text-info">New Project</h3>
				<form action="saveProject.php" method="post">
					<label class='col-sm-4'>Project name:
						<input  name='project_name' class='form-control' type='text'>
					</label>
					<label class='col-sm-4'>Project Start date:
						<input  name='project_startdate' class='form-control'  type='date'>
					</label>
					<label class='col-sm-4'>Project End date:
						<input  name='project_enddate' class='form-control' type='date'>
					</label>
					<label class='col-sm-4'>Customer:
						<?php
							echo $project->callCustomer('');
						?>
					</label>
					<label class='col-sm-4'>Status:
						<?php	
							echo $project->callStatus('');
						?>
					</label>

					<hr>
					<button class="btn btn-success">Save</button> <a href="index.php" class="btn btn-warning">Back</a>
				</form>
			</div>
		</div>
	</section>
</div>
<?php
	echo $h->footer();
?>
