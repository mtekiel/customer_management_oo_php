<?php
    require "class/database.class.php";
    require "class/status.class.php";
    require "class/general.class.php";
    require "class/html.class.php";

    $h=new HTMLPage;

    echo $h->head();
    echo $h->bodystart();
    echo $h->navbar();
    echo $h->aside();
?>
<div class="content-wrapper">
    <section class="content">
        <div class="card">
            <div class="card-body">
                <h3 class="text-info">Status</h3>
                <?php
                    $status= new Status();
                    $all= $status->getAll();
                    $general=new General();
                    echo $general->newbutton('New Status','createStatus.php','info' );
                ?>
                <table class="table">
                    <thead>
                        <th>#</th><th>Status</th><th>Color</th>
                    </thead>
                    <tbody>
                        <?php
                            foreach($all as $key=>$row){
                                foreach($row as $k=> $cell){
                                    echo "<td>$cell</td>";
                                        
                                    if($k=="status_id"){
                                        $id=$cell;
                                    }
                                }
                                echo "<td><a href='updateStatus.php?id=$id' class='btn btn-warning'>Update</a>
                                    <a href='deleteStatus.php?id=$id' class='btn btn-danger'>Delete</a></td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php
    echo $h->footer();
?>
