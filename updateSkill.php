<?php
    require "class/database.class.php";
    require "class/skill.class.php";
    require "class/general.class.php";
    require "class/html.class.php";

    $h=new HTMLPage;

    echo $h->head();
    echo $h->bodystart();
    echo $h->navbar();
    echo $h->aside();
?>
<div class="content-wrapper">
    <section class="content">
        <div class="card">
            <div class="card-body">
                <h3 class="text-info">Update Skill</h3>
                <form action="saveSkill.php?id=<?php echo $_GET['id'];?>" method="post">
                    <?php
                        $skill= new Skill();
                        $one=$skill->getOne($_GET['id']);                    
                    ?>

                    <input  name='skill_id' class='form-control' hidden value='<?php echo $one[0]['skill_id'];?>' type='text'>

                    <label class='col-sm-4'>Skill:<input  name='skill' class='form-control'  value='<?php echo $one[0]['skill'];?>' type='text'>
                    </label>
                    <hr>
                    <button class="btn btn-success">Save</button> <a href="skill.php" class="btn btn-warning">Back</a>
                </form>    
            </div>
    </section>
</div>
<?php
  echo $h->footer();
?>
