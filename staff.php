<?php
	require "class/database.class.php";
	require "class/staff.class.php";
	require "class/general.class.php";

	require "class/html.class.php";
	$h=new HTMLPage;
	echo $h->head();
	echo $h->bodystart();
	echo $h->navbar();
	echo $h->aside();
?>
<div class="content-wrapper">
	<section class="content">
		<div class="card">
			<div class="card-body">
				<h3 class="text-info">Staff</h3>
				<?php
					$staff= new Staff();
					$all= $staff->getAll();					
					$general=new General();
					echo $general->newbutton('New Staff','createStaff.php','info' );
					?>
					<table class="table">
						<thead>
							<th>#</th><th>Name</th><th>Deparment</th><th>Role</th><th>Team</th><th>Action</th>
						</thead>
						<tbody>
							<?php
								foreach($all as $key=>$row){
									foreach($row as $k=> $cell){
										echo "<td>$cell</td>";
										if($k=="staff_id"){
											$id=$cell;
										}
									}
									echo "<td><a href='updateStaff.php?id=$id' class='btn btn-warning'>Update</a>
										<a href='deleteStaff.php?id=$id' class='btn btn-danger'>Delete</a></td>";
									echo "</tr>";
								}
							?>
						</tbody>
				</table>
			</div>
		</div>
	</section>
</div>
<?php
  	echo $h->footer();
?>
