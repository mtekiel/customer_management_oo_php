<?php
    require "class/database.class.php";
    require "class/status.class.php";
    require "class/general.class.php";
    require "class/html.class.php";
    $h=new HTMLPage;
    echo $h->head();
    echo $h->bodystart();
    echo $h->navbar();
    echo $h->aside();
    $general= new General();
?>
<div class="content-wrapper">
    <section class="content">
        <div class="card">
            <div class="card-body">
                <h3 class="text-info">Insert Status</h3>
                <form action="saveStatus.php" method="post">
                    <label class='col-sm-4'>Statuts:
                        <input  name='status' class='form-control' type='text'>
                    </label>
                    <label class='col-sm-4'>Color:
                        <select class="form-control" name="color">
                            <?php
                                echo $general->color($one[0]['color']);
                            ?>
                        </select>
                    </label>

                    <hr>
                    <button class="btn btn-success">Save</button>
                    <a href="status.php" class="btn btn-warning">Back</a>
                </form>
            </div>
        </div>
    </section>
</div>
<?php
    echo $h->footer();
?>
