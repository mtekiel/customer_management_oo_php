<?php
	require "class/database.class.php";
	require "class/staff.class.php";
	require "class/skill.class.php";
	require "class/general.class.php";
	require "class/html.class.php";

	$h=new HTMLPage;

	echo $h->head();
	echo $h->bodystart();
	echo $h->navbar();
	echo $h->aside();
	
	$general = new General();
?>
<div class="content-wrapper">
	<section class="content">
		<div class="card">
			<div class="card-body">
				<h3 class="text-info">Update Staff</h3>
				<form action="doupStaff.php?id=<?php echo $_GET['id'];?>" method="post">
					<?php
						$staff= new Staff();						
						$one=$staff->getOne($_GET['id']);
						$sklii = new Skill();						
					?>
					<input  name='staff_id' class='form-control' hidden value='<?php echo $one[0]['staff_id'];?>' type='text'>

					<label class='col-sm-4'>Staff name:<input  name='staff_name' class='form-control'  value='<?php echo $one[0]['staff_name'];?>' type='text'>
					</label>
					<label class='col-sm-4'>Staff department:<input  name='staff_department' class='form-control'  value='<?php echo $one[0]['staff_department'];?>' type='text'>
					</label>
					<label class='col-sm-4'>Staff role:<input  name='staff_role' class='form-control'  value='<?php echo $one[0]['staff_role'];?>' type='text'>
					</label>
					<label class='col-sm-4'>Staff team:<input  name='staff_team' class='form-control'  value='<?php echo $one[0]['staff_team'];?>' type='text'>
					</label>
					<hr>
					<button class="btn btn-success">Save</button>  <a href="staff.php" class="btn btn-warning">Back</a>
					<hr>
					<?php
						
						echo "<h3 class='text-info'>Skills of Employee</h3>";
						$list=$staff->listSkill($_GET['id']);						
					?>
					<div class="row">
						<table class="table col-sm-6">
							<thead><th>Skill Name</th><th>Action</th>
							</thead>
							<tbody>
								<?php
									foreach($list as $key=>$row){
										foreach($row as $k=> $cell){
											if($k=="ss_id"){
												$ss_id=$cell;
												
											}else {
											echo "<td>$cell</td>";
											}
										}

										TODO: // Make delete option properly 
										
								?>								

									<td><input class='btn btn-danger' type="submit" formaction="unassignSkillFromStaff.php?id=<?php echo $_GET['id']."&ss_id=".$ss_id;?>" value="Delete Skill" /></td>										

								<?php
									//echo "<td> <button class='btn btn-danger' name='ss_id'>Delete Skill</button></td>";										
										echo "</tr>";
									}
									echo '<tr><td>';
										$id = $_GET['id'];											
										
										$skill= $staff->listAvailableSkills($id);
										echo $skill;										
								?>
									
									<input class='btn btn-primary' type="submit" formaction="assignSkillToStaff.php?id=<?php echo $_GET['id'];?>" value="Assign Skill" />	
										
									</td></tr>		
								
							</tbody>							
						</table>						
					</div>						
				</form>
			</div>
		</div>
	</section>
</div>
<?php
  echo $h->footer();
?>
