<?php
    require "class/database.class.php";
    require "class/status.class.php";
    require "class/general.class.php";
    require "class/html.class.php";

    $h=new HTMLPage;   
    echo $h->head();
    echo $h->bodystart();
    echo $h->navbar();
    echo $h->aside();
?>
<div class="content-wrapper">
    <section class="content">
        <div class="card">
            <div class="card-body">
                <h3 class="text-info">Insert Skill</h3>
                <form action="saveSkill.php" method="post">
                    <label class='col-sm-4'>Skill:
                        <input  name='skill' class='form-control' type='text'>
                    </label>
                    <hr>
                    <button class="btn btn-success">Save</button>
                    <a href="skill.php" class="btn btn-warning">Back</a>
                </form>
            </div>
        </div>
    </section>
</div>
<?php
    echo $h->footer();
?>
