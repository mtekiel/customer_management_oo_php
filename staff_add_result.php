<html>
<?php

//validate fields and return error messages

if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Validate Staff Name
    $input_staff_name = trim($_POST["staff_name"]);
    if(empty($input_staff_name)){
        $staff_name_err = "Please enter employee name";
    } elseif(!filter_var($input_staff_name, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $staff_name_err = "Please enter a valid name.";
    } else{
        $staff_name = $input_staff_name;
    }

    // Validate Staff Name
    $input_staff_department = trim($_POST["staff_department"]);
    if(empty($input_staff_department)){
        $staff_department_err = "Please enter Department";
    } elseif(!filter_var($input_staff_department, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $staff_department_err = "Please enter a valid Department.";
    } else{
        $staff_department = $input_staff_department;
    }

    // Validate Staff Role
    $input_staff_role = trim($_POST["staff_role"]);
    if(empty($input_staff_role)){
        $staff_role_err = "Please enter Role";
    } elseif(!filter_var($input_staff_role, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $staff_role_err = "Please enter a valid role.";
    } else{
        $staff_role = $input_staff_role;
    }

    // Validate Staff Team
    $input_staff_team = trim($_POST["staff_team"]);
    if(empty($input_staff_team)){
        $staff_team_err = "Please enter a Team";
    } elseif(!filter_var($input_staff_team, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $staff_team_err = "Please enter a valid team.";
    } else{
        $staff_team = $input_staff_team;
    }

    $input_skill_id = trim($_POST["skill_id"]);
    if(empty($input_skill_id)){
        $skill_id_err = "Please select a Skill ID";
    } elseif(!filter_var($input_skill_id, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[0-9\s]+$/")))){
        $skill_id_err = "Please select a valid skill_id.";
    } else{
        $skill_id = $input_skill_id;
    }

    include("staff_functions.php");
    // Check input errors before inserting in database
    if(empty($staff_name_err) && empty($staff_department_err) && empty($staff_role_err) && empty($staff_team_err) && empty($skill_id_err)) {
      $staff = new Staff();
      $staff->addStaff($staff_name, $staff_department, $staff_role, $staff_team, $skill_id);
    }
}

?>
</html>
