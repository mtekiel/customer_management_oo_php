<?php
    require "class/database.class.php";
    require "class/skill.class.php";
    require "class/html.class.php";
    require "class/general.class.php";

    $h=new HTMLPage;

    echo $h->head();
    echo $h->bodystart();
    echo $h->navbar();
    echo $h->aside();
?>
<div class="content-wrapper">
    <section class="content">
        <div class="card">
            <div class="card-body">
                <h3 class="text-info">Skills</h3>
                <?php
                    $skill= new Skill();
                    $all= $skill->getAll();
                    $general=new General();
                    echo $general->newbutton('New Skill','createSkill.php','info' );
                ?>
                <table class="table">
                    <thead>
                        <th>#</th><th>Skill</th>
                    </thead>
                    <tbody>
                        <?php
                            foreach($all as $key=>$row){
                                foreach($row as $k=> $cell){
                                    echo "<td>$cell</td>";
                                    
                                    if($k=="skill_id"){
                                        $id=$cell;
                                    }
                                }
                                echo "<td><a href='updateSkill.php?id=$id' class='btn btn-warning'>Update</a>
                                    <a href='deleteSkill.php?id=$id' class='btn btn-danger'>Delete</a></td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php
    echo $h->footer();
?>
