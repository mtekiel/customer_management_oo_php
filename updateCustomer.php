<?php
    require "class/database.class.php";
    require "class/customer.class.php";
    require "class/general.class.php";
    require "class/html.class.php";

    $h=new HTMLPage;

    echo $h->head();
    echo $h->bodystart();
    echo $h->navbar();
    echo $h->aside();
?>
<div class="content-wrapper">
    <section class="content">     
        <div class="card">
            <div class="card-body">
                <h3 class="text-info">Update Customer</h3>
                <form action="doupCustomer.php?id=<?php echo $_GET['id'];?>" method="post">
                    <?php
                        $customer= new Customer();
                        $one=$customer->getOne($_GET['id']);
                    ?>

                    <input  name='customers_id' class='form-control' hidden value='<?php echo $one[0]['customers_id'];?>' type='text'>

                    <label class='col-sm-4'>Customer name:<input  name='customer_name' class='form-control'  value='<?php echo $one[0]['customer_name'];?>' type='text'>
                    </label>
                    <label class='col-sm-4'>Customer Phone:<input  name='customer_email' class='form-control'  value='<?php echo $one[0]['customer_email'];?>' type='text'>
                    </label>
                    <label class='col-sm-4'>Staff role:<input  name='customer_phone' class='form-control'  value='<?php echo $one[0]['customer_phone'];?>' type='text'>
                    </label>

                    <hr>
                    <button class="btn btn-success">Save</button> <a href="customer.php" class="btn btn-warning">Back</a>
                </form>
            </div>
        </div>    
    </section>
</div>
<?php
    echo $h->footer();
?>
