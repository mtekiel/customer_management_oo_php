<?php
	require "class/database.class.php";
	require "class/staff.class.php";
	require "class/html.class.php";

	$h=new HTMLPage;

	echo $h->head();
	echo $h->bodystart();
	echo $h->navbar();
	echo $h->aside();

	$staff = new Staff();
		
	$assignSkill = $staff->addSkill($_GET['id'],$_POST['new_skill']);	
	if(isset($_POST['submit'])){
		echo "<meta http-equiv='refresh' content='0'>";
	}	
	
?>

<div class="content-wrapper">
	<section class="content">
		<div class="card">
			<div class="card-body">
				<?php
										
					echo "<p class='class='alert alert-success'>New Skill assigned to Staff with success!</p>";
						
				?>
				<a href= "javascript:history.back()" class="btn btn-primary">Back</a>
			</div>
		</div>
	</section>
</div>

<?php
	echo $h->footer();
?>
