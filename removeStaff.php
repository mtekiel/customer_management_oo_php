<?php
    require "class/database.class.php";
    require "class/project.class.php";
    require "class/html.class.php";
    $h=new HTMLPage;
    echo $h->head();
    echo $h->bodystart();
    echo $h->navbar();
    echo $h->aside();
    $project= new Project();

?>
<div class="content-wrapper">
    <section class="content">
        <div class="card">
            <div class="card-body">
            <?php
                $project->removeStaff($_GET['spid'] );
                echo "<p class='class='alert alert-success'>Person removed with success!</p>";
            ?>
                <a href= "javascript:history.back()" class="btn btn-primary">Back</a>
            </div>
        </div>
    </section>
</div>
<?php
    echo $h->footer();
?>
