<?php
class General{

    function __construct(){
        $this->db = new Database();
        $this->db->connect();
    }

    function timeline($array){
        $tr="";
        $tr.= "<table class='table  table-hover  table-bordered'>";
        $tr.= "<thead><th>Project</th><th>Start</th><th>End</th><th>Status</th></thead>";
        $tr.="<tbody>";

		foreach($array as $number => $number_array){
			foreach($number_array as $key =>$value){
				switch ($key){
					case 'status':
						$tr.="<td> $color $value</span></td>";
					break;
					case "color":
						$color="<span class='badge badge-".$value ."'>";
					break;
					default:
						$tr.="<td>$value</td>";
					break;
				}
			}
			$tr.="</tr>";
		}
		
		$tr.="</tbody>";
		$tr.= "</table>";
		return $tr;
    }
  
  	function color($v){
		$opt="";
		$color = array('primary' ,'info' ,'danger','warning', 'success', 'secondary');
		
		foreach($color as $key =>$value){
			if($v==$value) $s="selected"; else  $s="";
			$opt.="<option value='$value' $s>".ucfirst($value)."</option>";
		}
		return $opt;
  	}
  
	function newbutton($text,$link,$color){
		$b="<a href='$link' class='btn btn-$color'>$text</a>";
		return $b;
	}
	
	function taskcolor($color){
		switch ($color){
			case 'info':
				$color="007bff";
				break;
			case 'primary':
				$color="17a2b8";
				break;
			case 'secondary':
				$color="6c757d";
				break;
			case 'success':
				$color="28a745";
				break;
			case 'danger':
				$color="dc3545";
				break;
			case 'warning':
				$color="ffc107";
				break;
			default:
				$color="007bff";
				break;
		}
		return $color;
	}
	
	function gantt(){
		$q="Select project_name,DATE_FORMAT(project_startdate, '%d/%m/%Y') as start, DATE_FORMAT(project_enddate, '%d/%m/%Y') as end,
		status, project_id
		from projects left join status on projects.status = status.status_id ";
		$do= $this->db->query($q, "select");
		$task="";
		foreach ($do as $key => $value){
			$task.='{
					id: "'.$value['project_id'].'", text: "'.$value['project_name'].' - '.$value['status'].'", start_date: "'.$value['start'].'", end_date:"'.$value['end'].'",
					open:true, color:"#'.$this->taskcolor($value['color']).'"
					},';
		}
		return $task;
	}
}
