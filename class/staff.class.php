<?php
class Staff{
  private $db;

	function __construct(){
		$this->db = new Database();
		$this->db->connect();
	}

	function getAll(){
		$do= $this->db->query("select * from staff", "select");
		return $do;
	}

	function getOne($id){
		$do= $this->db->query("select * from staff where staff_id='$id'", "select");
		return $do;
	}

	function deletequery($id) {
		$do= $this->db->query("DELETE from staff where staff_id='$id' ", "");
	}

	function update($id, $arr){
		$q="Update staff set staff_name='$arr[staff_name]', staff_department='$arr[staff_department]',
		staff_role='$arr[staff_role]', staff_team='$arr[staff_team]' WHERE staff_id='$id'";
		$do= $this->db->query($q, "");
	}

	function insert ($arr){
		$q="insert into staff values (null,' $arr[staff_name]', '$arr[staff_department]', '$arr[staff_role]','$arr[staff_team]')";
		$do= $this->db->query($q, "");
	}

	function callSkill(){
		$q="select * from skills order by skill";
		$do_query= $this->db->query($q, "select");
		$o="<table class='table '><tr><td>";
		$o.="<select class='form-control' name ='new_skill'>";
		foreach ($do_query as $a) {			
			$o.="<option value='$a[skill_id]'>$a[skill]</option>";
		}
		$o.="</select></td><td>";		
		// $o.="<button class='btn btn-primary'>Assign Skill</button></td></tr></table>";
		
		return $o;
	}
	
	// list of skills which are not assigned yet to staff (by staff_id )
	function listAvailableSkills($id){
		$q="SELECT skill,skill_id FROM `skills` WHERE skill_id NOT IN (select skill_id from `staff_to_skills` where staff_id = $id)";

		$do_query= $this->db->query($q, "select");

		$o="<table class='table '><tr><td>";
		$o.="<select class='form-control' name ='new_skill'>";

		foreach ($do_query as $a) {			
			$o.="<option value='$a[skill_id]'>$a[skill]</option>";
		}
		$o.="</select></td><td>";		
				
		return $o;
	}

	//function for list all the skill of a staff member
	function listSkill($id){
		$q="SELECT skill, ss_id FROM staff_to_skills left join skills on skills.skill_id=staff_to_skills.skill_id where staff_to_skills.staff_id=$id";
		$do_query= $this->db->query($q, "select");
		return $do_query;
	}

	//funtion for add a skill to a staff member
	function addSkill($id,$skill){
		$q="insert into staff_to_skills values (null,$id, $skill)";
		$do_query=  $this->db->query($q, "");
	}

	//skill for delete a skill to a staff member
	function deleteSkill($ss_id){
		$q= "DELETE from staff_to_skills where ss_id='$ss_id' ";
		$do =  $this->db->query($q, "");
	}
}



