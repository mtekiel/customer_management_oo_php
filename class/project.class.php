<?php
class Project{
	private $db;
	
    function __construct(){
        $this->db = new Database();
        $this->db->connect();
	}
	
    function getAll(){
      	$do= $this->db->query("SELECT project_id, project_name, DATE_FORMAT(project_startdate, '%d/%m/%Y'), 
			DATE_FORMAT(project_enddate, '%d/%m/%Y') , customer_name, color, status FROM projects as p, customers as c, status as ps
			WHERE c.customer_id = p.customer_id AND p.status_id = ps.status_id", "select");
      	return $do;    	
	}
	
    function getOne($id){
		
		$q = "SELECT projects.project_id, projects.project_name,DATE_FORMAT(project_startdate, '%Y-%m-%d') as project_startdate, 
		DATE_FORMAT(project_enddate, '%Y-%m-%d') as project_enddate , customers.customer_name, customers.customer_id, status.status,
		status.status_id, projects.project_id FROM projects left join customers on projects.customer_id = customers.customer_id left join
		status on projects.status_id = status.status_id where project_id=$id";

		$do= $this->db->query($q, "select");
		return $do;
	}
		
    function deletequery($id) {
        $do= $this->db->query("DELETE from projects where project_id='$id' ", "");        
	}
	//delete projects and appropriate staff id's from staff_to_project by project_id
	function deleteFromStaffToProject($id) {        
        $do = $this->db->query("DELETE from staff_to_project where project_id='$id' ", "");
	}

	//delete projects and appropriate staff id's from staff_to_project by staff_id

	function deleteFromStaffToProjectByStaff($id) {        
        $do = $this->db->query("DELETE from staff_to_project where staff_id='$id' ", "");
	}
	
	
    function update($id, $arr){
		$q="Update projects set project_name='$arr[project_name]', project_startdate='$arr[project_startdate]',
		project_enddate='$arr[project_enddate]', customer_id='$arr[customer_id]',
		status_id='$arr[status_id]' WHERE project_id='$id'";		

		$do= $this->db->query($q, "");
	}
	
    function insert($arr){
		
		$q="insert into projects values (null,'$arr[project_name]', '$arr[project_startdate]', '$arr[project_enddate]', $arr[customer_id], $arr[status_id] )";
		
		$do= $this->db->query($q, "");
	}
	
    //select of all the customers, with the one selected
    function callCustomer($id){
    	$q="select * from customers order by customer_name";
      	$do_query= $this->db->query($q, "select");

		$o="<select class='form-control' name ='customer_id'>";
		foreach ($do_query as $a) {
			if($id==$a['customer_id']) $s="selected"; else  $s="";
			$o.="<option value='$a[customer_id]' $s>$a[customer_name]</option>";
		}

		$o.="</select>";	
		
      	return $o;
	}
	
    function callStatus($id){
    	$q="select * from status order by status";
		$do_query= $this->db->query($q, "select");

		$o="<select class='form-control' name ='status_id'>";
		foreach ($do_query as $a) {
			if($id==$a['status_id']) $s="selected"; else  $s="";
			$o.="<option value='$a[status_id]' $s>$a[status]</option>";
		}

		$o.="</select>";
		return $o;
    }
	// List of staff still available to add
    function callStaff($project){
		$q="select * from staff 
		where staff_id not in (select staff_id from staff_to_project where project_id='$project') 
		order by staff_name";
		$do_query= $this->db->query($q, "select");
		$o="<table class='table table col-sm-6'><tr><td>";
		$o.="<select class='form-control' name ='staff_id'>";
		foreach ($do_query as $a) {
			$o.="<option value='$a[staff_id]'>$a[staff_name]</option>";
		}

		$o.="</select></td><td><button class='btn btn-success'>Add</button></td></tr></table>";
		return $o;
    }

    //function for list all staff assigned to project with $id
    function listStaff($id){
        $q="SELECT staff_name, staff_department, staff_role, staff_team, id as sp_id  FROM staff_to_project 
			left join staff on staff_to_project.staff_id=staff.staff_id where project_id='$id'";
			
		$do_query= $this->db->query($q, "select");
		return $do_query;
    }

    //funtion for add a skill to a staff member
    function addStaff($id,$project){
		$q="insert into staff_to_project values (null,'$id', $project)";
		$do_query= $this->db->query($q, "");
    }

    //Remove staff from project where sp_id = $id   check if this function is on use  
    function removeStaff($id){
		$q= "DELETE from staff_to_project where id='$id' ";
		$do=$this->db->query($q, "");
    }
}

