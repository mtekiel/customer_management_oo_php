<?php
class Availability{
    private $db;

    function __construct(){
        $this->db = new Database();
        $this->db->connect();
    }

    function generatethDate($d){
        $th="";
        for($i=0; $i<$d; $i++){
            $th.="<th>".date("d-m-Y" , strtotime("+$i day"))."</th>";
        }           
        return $th;
    }

    function generateDate($d){

       // $th=date("d-m-Y" , strtotime("+$d day"));
        $th=date("Y-m-d" , strtotime("+$d day"));
        return $th;
    }

    function generateLine(){
        $do= $this->db->query("select staff_name,staff_id from staff order by staff_name","select");
        $tr="";
        
        foreach ($do as $value) {
            $tr.= "<tr>";
            foreach ($value as $k => $v) {
                if($k=="staff_id"){
                    $id=$v;
                }else{
                    $tr.= "<td>".$v."</td>";
                }
            }
            
            $d=6;

            for($i=0 ; $i<$d; $i++){
                $date = $this->generateDate($i);                
                
                $data = "SELECT count(project_id) as count FROM projects WHERE 
                        ( '$date' >= project_startdate and '$date' <= project_enddate ) and project_id in (select project_id from staff_to_project where staff_id=$id)";
                
                $dodata= $this->db->query($data, "select");
               
                $tr.="<td>".$dodata[0]['count']."</td>";               
                 
            }
            $tr.= "</tr>";
            
        }        
        return $tr;
    }
}
