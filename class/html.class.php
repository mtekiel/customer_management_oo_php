<?php
class HTMLPage{
   public $pageTitle = "Project";

   function head(){
		$head="<!DOCTYPE html>
		<html>
		<head>
			<meta charset='utf-8'>
			<meta http-equiv='X-UA-Compatible 'content='IE=edge'>
			<title></title>
			<meta name='viewport' content=width='device-width, initial-scale=1'>
			<link rel='stylesheet' href='plugins/fontawesome-free/css/all.min.css'>
			<link rel='stylesheet' href='https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css'>
			<link rel='stylesheet' href='dist/css/adminlte.min.css'>
			<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700' rel='stylesheet'>

			<link rel=\"stylesheet\" href=\"plugins/gantt/skins/dhtmlxgantt_meadow.css\">
		</head>";
		return $head;
   }

   function bodystart(){
		$b="<body class='hold-transition sidebar-mini layout-fixed'>
		<div class='wrapper'>";
		return $b;
   }

   function navbar(){
		$nav="<nav class='main-header navbar navbar-expand navbar-white navbar-light'>
		<ul class='navbar-nav>'
		<li class='nav-item'>
			<a class='nav-link' data-widget='pushmenu' href='#'><i class='fas fa-bars'></i></a>
		</li>
		</ul>
		</nav>";
		return $nav;
   }

   function aside(){
		$aside="<aside class='main-sidebar sidebar-dark-primary elevation-4'>
		<a href='index.php' class='brand-link'>
		<i class='fas fa-cookie-bite'></i>
		<span class='brand-text font-weight-light'>Customer Management</span>
		</a>

		<div class='sidebar'>
		<nav class='mt-2'>
			<ul class='nav nav-pills nav-sidebar flex-column' data-widget='treeview' role='menu' data-accordion='false'>
				<li class='nav-item'>
				<a href='index.php' class='nav-link'>
					<i class='nav-icon fas fa-th'></i>
					<p>Projects</p>
				</a>
				</li>
				<li class='nav-item'>
				<a href='staff.php' class='nav-link'>
					<i class='nav-icon fas fa-users'></i>
					<p>Staff</p>
				</a>
				</li>
				<li class='nav-item'>
				<a href='availability.php' class='nav-link'>
					<i class='nav-icon fas fa-clock'></i>
					<p>Availability</p>
				</a>
				</li>
				<li class='nav-item'>
				<a href='customer.php' class='nav-link'>
					<i class='nav-icon fas fa-coins'></i>
					<p>Customers</p>
				</a>
				</li>
				
				<li class='nav-item'>
				<a href='skill.php' class='nav-link'>
					<i class='nav-icon fas fa-star-half-alt'></i>
					<p>Skills</p>
				</a>
				</li>
				<li class='nav-item'>
				<a href='status.php' class='nav-link'>
					<i class='nav-icon fas fa-toggle-on'></i>
					<p>Status
					</p>
				</a>
				</li>
			</ul>
		</nav>
		</div>
		</aside>";
		return $aside;
   }
   
   function footer(){
		$footer="</div><footer class='main-footer'></footer>
		<aside class='control-sidebar control-sidebar-dark'></aside>
     	</div>

		<script src='plugins/jquery/jquery.min.js'></script>
		<script src='plugins/bootstrap/js/bootstrap.bundle.min.js'></script>
		<script src='dist/js/adminlte.min.js'></script>
		<script src='dist/js/demo.js'></script>
		<script src=\"plugins/gantt/dhtmlxgantt.js?v=6.3.7\"></script>
		</body>
		</html>";

        return $footer;
    }
}

