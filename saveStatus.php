<?php
    require "class/database.class.php";
    require "class/status.class.php";
    require "class/html.class.php";

    $h=new HTMLPage;

    echo $h->head();
    echo $h->bodystart();
    echo $h->navbar();
    echo $h->aside();

    $status= new Status();
    $d= $status->insert($_POST);
?>
<div class="content-wrapper">
    <section class="content">
        <div class="card">
            <div class="card-body">
                <?php
                    echo "<p class='class='alert alert-success'>New Status create with success!</p>";
                ?>
                <a href= "javascript:history.back()" class="btn btn-primary">Back</a>
            </div>
        </div>
    </section>
</div>
<?php
    echo $h->footer();
?>
